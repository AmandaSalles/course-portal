package com.amandasalles.epam.courseportal.repositories;

import com.amandasalles.epam.courseportal.entities.*;
import org.springframework.data.jpa.repository.*;

public interface AdministratorRepository extends JpaRepository<Administrator, Long> {
}
