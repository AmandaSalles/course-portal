package com.amandasalles.epam.courseportal.services;

import com.amandasalles.epam.courseportal.dto.*;
import com.amandasalles.epam.courseportal.entities.*;
import com.amandasalles.epam.courseportal.mappers.*;
import com.amandasalles.epam.courseportal.repositories.*;
import org.modelmapper.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.data.rest.webmvc.*;
import org.springframework.stereotype.*;
import org.springframework.ui.*;

import java.util.*;
import java.util.stream.*;

@Service
public class CourseService {

    @Autowired
    private CourseRepository repository;

    @Autowired
    private CourseMapper mapper;


    public CourseDTO findById(Long id) {
        Course entity = repository.findById(id).get();
        CourseDTO dto = mapper.toDTO(entity);
        return dto;
    }

    public List<CourseDTO> findAll() {
        List<Course> courses = repository.findAll();

        return mapper.toListDTO(courses);
    }

    public CourseDTO save(CourseDTO course) {
        Course entity = mapper.toEntity(course);
        return mapper.toDTO(repository.save(entity));
    }

    public CourseDTO update(Long id, CourseDTO dto){
        if(!repository.existsById(id)){
            throw new ResourceNotFoundException();
        }
        dto.setId(id);
        return save(dto);
    }

    public void delete(Long id) {
        Optional<Course> course = repository.findById(id);
        if(course.isEmpty()){
            throw new ResourceNotFoundException();
        }
        Course entity = course.get();

        repository.delete(entity);

    }


}
