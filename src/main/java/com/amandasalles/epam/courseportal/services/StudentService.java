package com.amandasalles.epam.courseportal.services;

import com.amandasalles.epam.courseportal.dto.*;
import com.amandasalles.epam.courseportal.entities.*;
import com.amandasalles.epam.courseportal.mappers.*;
import com.amandasalles.epam.courseportal.repositories.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.data.rest.webmvc.*;
import org.springframework.stereotype.*;

import java.util.*;

@Service
public class StudentService {

    @Autowired
    private StudentRepository repository;

    @Autowired
    private StudentMapper mapper;

    public StudentDTO findById(Long id) {
        Student entity = repository.findById(id).get();
        StudentDTO dto = mapper.toDTO(entity);
        return dto;
    }

    public List<StudentDTO> findAll() {
        List<Student> students = repository.findAll();

        return mapper.toListDTO(students);
    }

    public StudentDTO save(StudentDTO student) {
        Student entity = mapper.toEntity(student);
        return mapper.toDTO(repository.save(entity));
    }

    public StudentDTO update(Long id, StudentDTO dto){
        if(!repository.existsById(id)){
            throw new ResourceNotFoundException();
        }
        dto.setId(id);
        return save(dto);
    }

    public void delete(Long id) {
        Optional<Student> student = repository.findById(id);
        if(student.isEmpty()){
            throw new ResourceNotFoundException();
        }
        Student entity = student.get();

        repository.delete(entity);

    }
}
