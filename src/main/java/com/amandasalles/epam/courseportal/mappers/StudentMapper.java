package com.amandasalles.epam.courseportal.mappers;

import com.amandasalles.epam.courseportal.dto.*;
import com.amandasalles.epam.courseportal.entities.*;
import org.modelmapper.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;

import java.util.*;
import java.util.stream.*;

@Component
public class StudentMapper {

    @Autowired
    private ModelMapper mapper;

    public StudentDTO toDTO(Student student) {
        StudentDTO studentDTO = mapper.map(student, StudentDTO.class);
        return studentDTO;
    }

    public Student toEntity(StudentDTO studentDTO){
        Student student = mapper.map(studentDTO, Student.class);
        return student;
    }

    public List<StudentDTO> toListDTO(List<Student> students){
        return students.stream().map(student -> toDTO(student)).collect(Collectors.toList());
    }

    public List<Student> toListEntityList(List<StudentDTO> students) {
        return students.stream().map(student -> toEntity(student)).collect(Collectors.toList());
    }
}
