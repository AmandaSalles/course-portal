package com.amandasalles.epam.courseportal.mappers;

import com.amandasalles.epam.courseportal.dto.*;
import com.amandasalles.epam.courseportal.entities.*;
import org.modelmapper.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;

import java.util.*;
import java.util.stream.*;

@Component
public class CourseMapper {
    @Autowired
    private ModelMapper mapper;

    public CourseDTO toDTO(Course course) {
        CourseDTO courseDTO = mapper.map(course, CourseDTO.class);
        courseDTO.setNumberOfStudents(course.getRegisters().size());
        return courseDTO;
    }

    public Course toEntity(CourseDTO courseDTO) {
        Course course = mapper.map(courseDTO, Course.class);
        return course;
    }

    public List<CourseDTO> toListDTO(List<Course> courses) {
        return courses.stream().map(course -> toDTO(course)).collect(Collectors.toList());
    }

    public List<Course> toListEntity(List<CourseDTO> courses) {
        return courses.stream().map(course -> toEntity(course)).collect(Collectors.toList());
    }
}
