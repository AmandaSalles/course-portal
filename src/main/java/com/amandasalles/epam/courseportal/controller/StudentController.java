package com.amandasalles.epam.courseportal.controller;

import com.amandasalles.epam.courseportal.dto.*;
import com.amandasalles.epam.courseportal.mappers.*;
import com.amandasalles.epam.courseportal.services.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(value = "/student")
public class StudentController {

    @Autowired
    private StudentService service;

    @Autowired
    private StudentMapper mapper;

    @GetMapping(value = "/{id}")
    public StudentDTO findById(@PathVariable Long id) {
        StudentDTO result = service.findById(id);
        return result;
    }

    @PostMapping
    public StudentDTO save(@RequestBody StudentDTO student){
        StudentDTO result = service.save(student);
        return result;
    }

    @PutMapping(value = "/{id}")
    public StudentDTO update(@PathVariable Long id, @RequestBody StudentDTO student){
        StudentDTO result = service.update(id, student);
        return result;
    }

    @DeleteMapping(value = "/{id}")
    public void delete(@PathVariable Long id){
        service.delete(id);
    }


}
