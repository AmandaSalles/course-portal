package com.amandasalles.epam.courseportal.controller;

import com.amandasalles.epam.courseportal.dto.*;
import com.amandasalles.epam.courseportal.mappers.*;
import com.amandasalles.epam.courseportal.services.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping(value = "/course")
public class CourseController {

    @Autowired
    private CourseService service;

    @Autowired
    private CourseMapper mapper;


    @GetMapping
    public List<CourseDTO> findAll() {
        return service.findAll();
    }

    @GetMapping(value = "/{id}")
    public CourseDTO findById(@PathVariable Long id) {
        CourseDTO result = service.findById(id);
        return result;
    }

    @PostMapping
    public CourseDTO save(@RequestBody CourseDTO course){
       CourseDTO result = service.save(course);
       return result;
    }

    @PutMapping(value = "/{id}")
    public CourseDTO update(@PathVariable Long id, @RequestBody CourseDTO course){
        CourseDTO result = service.update(id, course);
        return result;
    }

    @DeleteMapping(value = "/{id}")
    public void delete(@PathVariable Long id){
        service.delete(id);
    }
}
