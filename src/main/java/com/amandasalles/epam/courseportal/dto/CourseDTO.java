package com.amandasalles.epam.courseportal.dto;

import com.amandasalles.epam.courseportal.entities.*;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CourseDTO {

    private Long id;
    private String nameCourse;
    private int durationWeeks;
    private Teacher teacher;
    private Topic principalTopic;
    private int numberOfStudents;
}
