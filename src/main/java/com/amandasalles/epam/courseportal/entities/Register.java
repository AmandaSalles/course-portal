package com.amandasalles.epam.courseportal.entities;

import javax.persistence.*;
import java.time.*;

@Entity
public class Register {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    //TODO remember to fix this, composite foreign key

   @ManyToOne
   @JoinColumn(name = "student_id")
   private Student student;

   @ManyToOne
   @JoinColumn(name = "course_id")
   private Course course;


    private Double grade;
    private LocalDate startAt;
    private LocalDate completeAt;


}
