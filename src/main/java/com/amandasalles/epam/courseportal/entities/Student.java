package com.amandasalles.epam.courseportal.entities;

import lombok.*;

import javax.persistence.*;
import java.util.*;

@Getter
@Setter
@Entity
@DiscriminatorValue("student")
public class Student extends User {
    private Boolean isBlocked;

    @OneToMany(mappedBy = "student")
    private Set<Register> registers;

}
