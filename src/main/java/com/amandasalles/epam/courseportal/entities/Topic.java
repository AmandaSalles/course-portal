package com.amandasalles.epam.courseportal.entities;

import lombok.*;

import javax.persistence.*;
import java.util.*;

@Getter
@Setter
@Entity
@Table(name = "topic")
public class Topic {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nameTopic;

    @OneToMany
    @JoinColumn(name = "course_id")
    private List<Course> courses;
}
