package com.amandasalles.epam.courseportal.entities;

import lombok.*;

import javax.persistence.*;
import java.util.*;

@Getter
@Setter
@Entity
@DiscriminatorValue("teacher")
public class Teacher extends User{

    @OneToMany
    @JoinColumn(name = "course_id")
    private List<Course> courses;

}
