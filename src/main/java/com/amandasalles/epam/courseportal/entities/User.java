package com.amandasalles.epam.courseportal.entities;

import lombok.*;

import javax.persistence.*;
import java.time.*;

@Getter
@Setter
@Entity(name = "users")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "role" , discriminatorType = DiscriminatorType.STRING)
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String email;
    private String password;
    private String firstName;
    private String lastName;
    private LocalDate birthday;
}
