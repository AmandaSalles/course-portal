package com.amandasalles.epam.courseportal.entities;

import lombok.*;

import javax.persistence.*;
import java.util.*;

@Getter
@Setter
@Entity
public class Course {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String nameCourse;
    private int durationWeeks;

    @ManyToOne
    private Teacher teacher;

    @ManyToOne
    private Topic principalTopic;

    @OneToMany(mappedBy = "course")
    private Set<Register> registers;
}
