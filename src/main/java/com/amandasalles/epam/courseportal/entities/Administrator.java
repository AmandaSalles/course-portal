package com.amandasalles.epam.courseportal.entities;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Entity
@DiscriminatorValue("admin")
public class Administrator extends User{

}
